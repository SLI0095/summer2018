#pragma once
struct Node
{
	int value;
	Node* left;
	Node* right;

	Node(int value);
	~Node();

	bool IsLeaf();
	bool HasLeft();
	bool HasRight();

	static bool CompareStructure(Node *a, Node *b);
};

